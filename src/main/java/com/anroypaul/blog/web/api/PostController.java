package com.anroypaul.blog.web.api;

import com.anroypaul.blog.domain.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.anroypaul.blog.domain.PostRepository;

@RestController
@RequestMapping(path = PostController.API_POSTS)
public class PostController {

    static final String API_POSTS = "/api/posts";

    private final PostRepository postRepository;

    @Autowired
    public PostController(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Iterable<Post> getAllPosts() {
        return postRepository.findAll();
    }

    @RequestMapping(value = "/{postId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Post findPost(@PathVariable Long postId) {
        return postRepository.findOne(postId);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Post createPost(@RequestBody Post post) {
        return postRepository.save(post);
    }

    @RequestMapping(value = "/{postId}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Post updatePost(@PathVariable Long postId, @RequestBody Post post) {
        return postRepository.save(post);
    }

    @RequestMapping(value = "/{postId}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deletePost(@PathVariable Long postId) {
        postRepository.delete(postId);
    }

}

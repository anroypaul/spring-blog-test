package com.anroypaul.blog.service;

import com.anroypaul.blog.domain.User;
import com.anroypaul.blog.domain.UserRepository;
import org.springframework.stereotype.Service;

public interface UserService {

    User registerNewUser(User user);
}

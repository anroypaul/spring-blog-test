package com.anroypaul.blog.config;

import com.anroypaul.blog.domain.Post;
import com.anroypaul.blog.domain.User;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

@Configuration
public class RepositoryRestConfig extends RepositoryRestMvcConfiguration {

    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Post.class);
        config.exposeIdsFor(User.class);
    }
}
